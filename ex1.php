<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora</title>
</head>
<body>
    <h1>La meva calculadora</h1>
    <form action="#" method="get"> <!-- funciona amb get i post -->
        <p>Numero 1: <input type="text" name="num1" required></p>

        <p>Numero 2: <input type="text" name="num2" required></p>

        <input type="submit" value="Calcula">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "GET") { //si es per get llegeix els parametres de get

        $num1 = $_GET["num1"];
        $num2 = $_GET["num2"];

    } elseif (_SERVER["REQUEST_METHOD"] == "POST") { //si es per post llegeix els parametres de post

        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];

    }
    if (($num1 === null  || $num2 === null) ){ // si algun dels dos numeros esta buit atura el programa
        
        echo "<p>Introdueix els dos numeros a operar</p>";
        exit;

    } elseif (!is_numeric($num1) || !is_numeric($num2)) { // si algun dels dos numeros no es numeric atura el programa

        echo "<p>Introdueix valors numerics</p>";
        exit;

    }
    else{

        echo "<p>$num1 + $num2 = ".$num1+$num2."</p>";
        echo "<p>$num1 - $num2 = ".$num1-$num2."</p>";
        echo "<p>$num1 x $num2 = ".$num1*$num2."</p>";

        if ($num1 == 0 || $num2 == 0){ // si una de les dos variables es 0 -> no executar la divisio pq peta
            echo "<p>$num1 / $num2 = NO ES POT DIVIDIR PER 0!!!</p>";
        }else{
            echo "<p>$num1 / $num2 = ".$num1/$num2."</p>";
        }
    }
    ?>
</body>
</html>